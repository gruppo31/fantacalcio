package Entity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * Rosa � una classe entity che memorizza la rosa del fantallenatore leggendola da file.  
 *
 * @author gruppo 3
 * @version 2
 * @since 16/7/2020
 *
 */

public class Rosa {

	private ArrayList<Giocatore> giocatori_rosa;

	public Rosa() {
		this.giocatori_rosa = new ArrayList <Giocatore>();
	}

	public void lettura_rosa (String file) throws IOException {
		
		try {
		Scanner reader = new Scanner(new File(file));

		String nome, cognome, squadra;
		int gol_subiti, ammonizioni, gol_segnati, assist;
		String ruolo="";

		boolean finito=false;
		while(!finito) {
			ruolo = reader.next();
			switch (ruolo) {
			case "PORTIERI": 
				for (int i=0; i<3; i++) {
					nome= reader.next();
					cognome= reader.next();
					squadra= reader.next();
					gol_subiti=reader.nextInt();
					giocatori_rosa.add(new Portiere (nome, cognome, squadra, gol_subiti));
				}
				break;
			case "DIFENSORI": 
				for (int i=0; i<9; i++) {
					nome= reader.next();
					cognome= reader.next();
					squadra= reader.next();
					ammonizioni=reader.nextInt();
					giocatori_rosa.add(new Difensore (nome, cognome, squadra, ammonizioni));
				}
				break;
			case "ATTACCANTI": 
				for (int i=0; i<7; i++) {
					nome= reader.next();
					cognome= reader.next();
					squadra= reader.next();
					gol_segnati=reader.nextInt();
					giocatori_rosa.add(new Attaccante (nome, cognome, squadra,gol_segnati));
				}
				finito=true;
				break;
			case "CENTROCAMPISTI": 
				for (int i=0; i<9; i++) {
					nome= reader.next();
					cognome= reader.next();
					squadra= reader.next();
					assist=reader.nextInt();
					giocatori_rosa.add(new Centrocampista (nome, cognome, squadra, assist));
				}
				break;
			}

		}

		} catch (IOException x) {
			 System.err.println(x);
	} 
		

	}



	public ArrayList<Giocatore> getGiocatori_rosa() {
		return giocatori_rosa;
	}

	public void setGiocatori_rosa(ArrayList<Giocatore> giocatori_rosa) {
		this.giocatori_rosa = giocatori_rosa;
	}

public void stampa_rosa() {
	for (int i=0; i<this.getGiocatori_rosa().size(); i++) {
		System.out.println(this.getGiocatori_rosa().get(i).cognome);
		System.out.println(this.getGiocatori_rosa().get(i).nome);
		System.out.println(this.getGiocatori_rosa().get(i).squadra);}
	}

}