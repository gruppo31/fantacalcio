package Entity;

import Entity.Giocatore;

/**
 * Attaccante � una classe entity che specializza giocatore e definisce la propria funzione calcola punteggio.
 *
 * @author gruppo 3
 * @version 2
 * @since 16/7/2020
 *
 */
public class Attaccante extends Giocatore {
	
	private String ruolo;
	private int gol_segnati;
	

	public Attaccante (String nome, String cognome, String squadra) {
		super(nome, cognome, squadra, 0);
		this.ruolo = "Attaccante";
		this.voto=0; 
		this.gol_segnati=0;
	}
	
	public Attaccante (String nome, String cognome, String squadra, int gol_segnati) {
		super(nome, cognome, squadra, 0);
		this.ruolo = "Attaccante";
		this.voto=0; 
	}
	
	@Override
	public double calcola_punteggio() {
		return voto = 6 + 3+ gol_segnati;	
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}

	public double getPunteggio() {
		return voto;
	}

	public void setPunteggio(double punteggio) {
		this.voto = punteggio;
	}

	public double getGol_subiti() {
		return gol_segnati;
	}

	public void setGol_subiti(int gol_subiti) {
		this.gol_segnati = gol_subiti;
	}

	
	
}
