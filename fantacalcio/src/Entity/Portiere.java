package Entity;
/**
 * Portiere � una classe entity che specializza giocatore e definisce la propria funzione calcola punteggio.
 *
 * @author gruppo 3
 * @version 2
 * @since 16/7/2020
 *
 */
public class Portiere extends Giocatore {
	
	private String ruolo;

	private double gol_subiti;
	

	public Portiere(String nome, String cognome, String squadra) {
		super(nome, cognome, squadra, 0);
		this.ruolo = "Portiere";
		this.voto=0; 
		this.gol_subiti=0;
	}
	
	public Portiere(String nome, String cognome, String squadra, int gol_subiti) {
		super(nome, cognome, squadra, 0);
		this.ruolo = "Portiere";
		this.voto=0; 
	}
	
	@Override
	public double calcola_punteggio() {
		return voto = 6-gol_subiti;	
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}

	public double getPunteggio() {
		return voto;
	}

	public void setPunteggio(double punteggio) {
		this.voto = punteggio;
	}

	public double getGol_subiti() {
		return gol_subiti;
	}

	public void setGol_subiti(double gol_subiti) {
		this.gol_subiti = gol_subiti;
	}

	
	
}
