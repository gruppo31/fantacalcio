package Entity;



import java.io.*;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;


/**
 * Giocatore � una classe astratta di tipo entity che memorizza le caratteristiche di un giocatore.
 * Presenta il metodo astratto calcola_punteggio che ogni classe che la estende implementa 
 *
 * @author gruppo 3
 * @version 2
 * @since 16/7/2020
 *
 */

public abstract class Giocatore {
	
	protected String nome;
	protected String cognome;
	protected String squadra;
	protected double voto;

	public Giocatore(String nome, String cognome, String squadra, double voto) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.squadra = squadra;
		this.voto= voto;
	}

	public double getVoto() {
		return voto;
	}

	public void setVoto(double voto) {
		this.voto = voto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getSquadra() {
		return squadra;
	}

	public void setSquadra(String squadra) {
		this.squadra = squadra;
	}
	
	public abstract double calcola_punteggio ();
	
	//metodo equals
	public boolean equals (Giocatore g) {
		return this.getNome().equals(g.getNome()) && this.getCognome().equals(g.getCognome()) && this.getSquadra().equals(g.getSquadra());
	}
	
//	public void salva_giocatore(String filename) throws IOException {
//		try {
//			FileWriter printout = new FileWriter(filename, true);
//			
//			printout.append(nome + " " + cognome + " " + squadra + "\n");
//			
//			printout.close();
//		} catch (IOException e) {
//			// Errore scrittura su file
//			e.printStackTrace();
//		}
//		
//	}
	public void salva_giocatore (String filename) throws IOException {// problema che me ne stampa uno per volta. avvisali.
		try {
			FileWriter w= new FileWriter (filename);
			BufferedWriter b = new BufferedWriter (w);
			PrintWriter printout = new PrintWriter (b);
			printout.append(nome + " " + cognome + " " + squadra + " " + "\n");
			printout.close();	
		}
		
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
