package Entity;

import Entity.Giocatore;
/**
 * Difensore � una classe entity che specializza giocatore e definisce la propria funzione calcola punteggio.
 *
 * @author gruppo 3
 * @version 2
 * @since 16/7/2020
 *
 */
public class Difensore extends Giocatore {
	
	private String ruolo;
	private int ammonizioni;
	

	public Difensore (String nome, String cognome, String squadra) {
		super(nome, cognome, squadra, 0);
		this.ruolo = "Difensore";
		this.voto=0; 
		this.ammonizioni=0;
	}
	
	public Difensore (String nome, String cognome, String squadra, int ammonizioni) {
		super(nome, cognome, squadra, 0);
		this.ruolo = "Difensore";
		this.voto=0; 
	}
	
	@Override
	public double calcola_punteggio() {
		return voto = 6 - 0.5*ammonizioni;	
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}

	public double getPunteggio() {
		return voto;
	}

	public void setPunteggio(double punteggio) {
		this.voto = punteggio;
	}

	public int getAmmonizioni() {
		return ammonizioni;
	}

	public void setAmmonizioni(int ammonizioni) {
		this.ammonizioni = ammonizioni;
	}

	
	
	
}
