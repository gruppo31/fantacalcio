package Entity;

//import java.io.BufferedWriter;

//import java.io.FileWriter;
//import java.io.IOException;
import java.util.ArrayList;

/**
 * Formazione � una entity che memorizza una collezione di giocatori, ossia la formazione 
 *
 * @author gruppo 3
 * @version 2
 * @since 16/7/2020
 *
 */

public class Formazione {

	
	private ArrayList<Giocatore> titolari=new ArrayList<Giocatore>();//vettore con la formazione

	public Formazione() {
		super();
		
	}

	public ArrayList<Giocatore> getTitolari() {
		return titolari;
	}

	public void setTitolari(ArrayList<Giocatore> titolari) {
		this.titolari = titolari;
	}
}
