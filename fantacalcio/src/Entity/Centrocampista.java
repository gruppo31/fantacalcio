package Entity;

import Entity.Giocatore;
/**
 * Centrocampista � una classe entity che specializza giocatore e definisce la propria funzione calcola punteggio.
 *
 * @author gruppo 3
 * @version 2
 * @since 16/7/2020
 *
 */
public class Centrocampista extends Giocatore {
	
	private String ruolo;
	private int assist;
	

	public Centrocampista (String nome, String cognome, String squadra) {
		super(nome, cognome, squadra, 0);
		this.ruolo = "Centrocampista";
		this.voto=0; 
		this.assist=0;
	}
	
	public Centrocampista (String nome, String cognome, String squadra, int assist) {
		super(nome, cognome, squadra, 0);
		this.ruolo = "Centrocampista";
		this.voto=0; 
	}
	
	@Override
	public double calcola_punteggio() {
		return voto = 6 + 1+ assist;	
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}

	public double getPunteggio() {
		return voto;
	}

	public void setPunteggio(double punteggio) {
		this.voto = punteggio;
	}

	public int getAssist() {
		return assist;
	}

	public void setAssist(int assist) {
		this.assist = assist;
	}

	

	
	
	
}
