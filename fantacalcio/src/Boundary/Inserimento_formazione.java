package Boundary;

import java.util.ArrayList;
import java.util.Scanner;

import Entity.Attaccante;
import Entity.Centrocampista;
import Entity.Difensore;
import Entity.Giocatore;
import Entity.Portiere;
import Entity.Rosa;
/**
 * Inserimento_formazione � una classe boundary che gestisce l'inserimento di un giocatore e ha dei metodi per migliorarne la visibilit�
 *
 * @author gruppo 3
 * @version 2
 * @since 16/7/2020
 *
 */

public class Inserimento_formazione {

	// boundary che chiede nome, conome e squadra
	static Scanner scan = new Scanner(System.in);
	String nome="";
	String cognome=""; 
	String squadra="";
	
	public Inserimento_formazione() {
		super();

	}

	public void dichiara_modulo () {System.out.println("Il modulo che inserisci � 3-4-3");}
	
	public void chiedi_portiere () {System.out.println("Inserisci il portiere schierato");}
	
	public void chiedi_difensori () {System.out.println("Inserisci uno per volta i tre difensori schierati");}
	
	public void chiedi_attaccanti () {System.out.println("Inserisci uno per volta i tre attaccanti schierati");}
	
	public void chiedi_centrocampisti () {System.out.println("Inserisci uno per volta i quattro centrocampisti schierati");}
	
	public void terminato () {System.out.println("Hai completato l'inserimento della formazione");}
	
	public String getNome() {
		System.out.println ("Inserisci il nome del giocatore che vuoi inserire tra i titolari:");
		nome=scan.next();
		
		return nome;
	}



	public String getCognome() {
		System.out.println ("Inserisci il cognome del giocatore che vuoi inserire tra i titolari:");
		cognome=scan.next();
		
		return cognome;
	}




	public String getSquadra() {
		System.out.println ("Inserisci la squadra del giocatore che vuoi inserire tra i titolari:");
		squadra=scan.next();
		
		return squadra;
	}





}
