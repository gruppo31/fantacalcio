package Boundary;

import java.util.Scanner;

/**
 * Menu_iniziale � una classe boundary che si occupa di interfacciarsi con l'utente chiedendogli a video cosa vuole fare. 
 *
 * @author gruppo 3
 * @version 2
 * @since 16/7/2020
 *
 */


public class Menu_iniziale {
	Scanner scan= new Scanner(System.in);	
	
	public int scelta_utente() {
		
		System.out.println("cosa vuoi fare? \n\t1 - inserisci formazione \n\t2 - visualizza punteggio \n\t3 - logout ");
		int scelta= scan.nextInt();
		return scelta;
	}

	public void scelte_vietate () {System.out.println("Questo caso d'uso non � stato implementato");}
	
	public int formazione_inseribile () {System.out.println("Mancano pi� di 10 minuti alla partita? true=1 o false=2");
	int right=scan.nextInt(); 
	return right;}
	
	public void tempo_scaduto () {System.out.println("Non puoi pi� inserire la formazione");}
}
