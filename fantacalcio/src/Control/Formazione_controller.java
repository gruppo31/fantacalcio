package Control;

import java.io.IOException;

import Boundary.Inserimento_formazione;
import Entity.Attaccante;
import Entity.Centrocampista;
import Entity.Difensore;
import Entity.Formazione;
import Entity.Giocatore;
import Entity.Portiere;
import Entity.Rosa;

/**
 * Formazione_controller � una classe di tipo Control che viene richiamata da Control_main che gestisce il caso d'uso di inserimento 
 * della formazione. Interagisce con l'utente per chiedere in ingresso la formazione e controlla la validit� di questo inserimento.
 * Successivamente stampa la formazione su di un file.
 * 
 *  
 * 
 * @author gruppo 3
 * @version 2
 * @since 16/7/2020
 *
 */

public class Formazione_controller {


	private Rosa rosa;
	private Formazione formazione_titolari;//alla fine creo un oggetto di tipo Formazione

	public Formazione_controller() throws IOException {//nel suo costruttore mi leggo la rosa tramite il metodo lettura_rosa
		super();
		this.formazione_titolari = new Formazione();
		this.rosa = new Rosa();
			try {
				this.rosa.lettura_rosa("C:\\Users\\fborr\\OneDrive\\Desktop\\GarraCharrua.txt");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	public void inserisci_formazione() {
		//rosa.stampa_rosa();
		String nome="";
		String cognome="";
		String squadra="";

		Inserimento_formazione inserimento_formazione = new Inserimento_formazione();
		inserimento_formazione.dichiara_modulo();
		int cnt=0;
		boolean uguali=false;
		
		do {
			inserimento_formazione.chiedi_portiere();
			nome = inserimento_formazione.getNome();
			cognome = inserimento_formazione.getCognome();
			squadra = inserimento_formazione.getSquadra();
			
			Giocatore portiere = new Portiere (nome, cognome, squadra);

			int k=0;
			uguali=false;
			while (k<rosa.getGiocatori_rosa().size() && !uguali) {//controlla se � nella rosa. se s�, cnt va ad 1
				uguali=portiere.equals(rosa.getGiocatori_rosa().get(k));
				k++;
			}
			
			if(uguali) {
				cnt++;
				formazione_titolari.getTitolari().add(portiere);
			} else {
				System.out.println("il giocatore non � nella tua rosa. scegline un altro ");
			}
		} while (cnt<1);

		cnt = 0;
		
		inserimento_formazione.chiedi_difensori();
		for (int p=0; p<3; p++) {//mi chiede i tre difensori
			do {
				nome = inserimento_formazione.getNome();
				cognome = inserimento_formazione.getCognome();
				squadra = inserimento_formazione.getSquadra();
	
				Giocatore difensore = new Difensore (nome, cognome, squadra);
	
				int k=0;
				uguali=false;
				while (k<rosa.getGiocatori_rosa().size() && !uguali) {//controlla se � nella rosa. se s�, cnt va ad 1
					uguali=difensore.equals(rosa.getGiocatori_rosa().get(k));
					k++;
				}
				
				if(uguali) {
					cnt++;
					formazione_titolari.getTitolari().add(difensore);
				} else {
					System.out.println("il giocatore non � nella tua rosa. scegline un altro ");
				}
			} while (cnt<1);
		}

	inserimento_formazione.chiedi_centrocampisti();
	for (int p=0; p<4; p++) {//mi chiede i 4 centroc.
		do {
			nome = inserimento_formazione.getNome();
			cognome = inserimento_formazione.getCognome();
			squadra = inserimento_formazione.getSquadra();

			Giocatore centrocampista = new Centrocampista (nome, cognome, squadra);

			int k=0;
			uguali=false;
			while (k<rosa.getGiocatori_rosa().size() && !uguali) {//controlla se � nella rosa. se s�, cnt va ad 1
				uguali=centrocampista.equals(rosa.getGiocatori_rosa().get(k));
				k++;
			}
			
			if(uguali) {
				cnt++;
				formazione_titolari.getTitolari().add(centrocampista);
			} else {
				System.out.println("il giocatore non � nella tua rosa. scegline un altro ");
			}
		} while (cnt<1);
	}
	
	
	inserimento_formazione.chiedi_attaccanti();
	for (int p=0; p<3; p++) {//mi chiede i tre difensori
		do {
			nome = inserimento_formazione.getNome();
			cognome = inserimento_formazione.getCognome();
			squadra = inserimento_formazione.getSquadra();

			Giocatore attaccante = new Attaccante (nome, cognome, squadra);

			int k=0;
			uguali=false;
			while (k<rosa.getGiocatori_rosa().size() && !uguali) {//controlla se � nella rosa. se s�, cnt va ad 1
				uguali=attaccante.equals(rosa.getGiocatori_rosa().get(k));
				k++;
			}
			
			if(uguali) {
				cnt++;
				formazione_titolari.getTitolari().add(attaccante);
			} else {
				System.out.println("il giocatore non � nella tua rosa. scegline un altro ");
			}
		} while (cnt<1);
	}
	
	inserimento_formazione.terminato();
	
	
	}
	
//	public void stampavideoformazione() {//DEBUG 
//		for (int i=0; i<this.formazione_titolari.getTitolari().size(); i++) {
//			System.out.println(this.formazione_titolari.getTitolari().get(i).getCognome());
//			System.out.println(this.formazione_titolari.getTitolari().get(i).getNome());
//			System.out.println(this.formazione_titolari.getTitolari().get(i).getSquadra());}
//		}
		
	
	
	
//	public void salva_formazione(String filename) throws IOException {
//		for(Giocatore giocatore : formazione_titolari.getTitolari()) {//foreach: per tutti i giocatori della formazione
//			giocatore.salva_giocatore(filename);//salvali
//		}
//	}
	
	public void salva_formazione (String filename) throws IOException {
		for (int i=0; i<formazione_titolari.getTitolari().size(); i++) {
			//System.out.println(i);
			formazione_titolari.getTitolari().get(i).salva_giocatore(filename);
		}
	}
	
//	public void punteggio_formazione() { //In futuro si implementer�
//		
//	}

	public Formazione getFormazione_titolari() {
		return formazione_titolari;
	}

	public void setFormazione_titolari(Formazione formazione_titolari) {
		this.formazione_titolari = formazione_titolari;
	}
	
	
}
