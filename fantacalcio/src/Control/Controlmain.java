package Control;

import java.io.IOException;
import java.util.ArrayList;

import Boundary.Menu_iniziale;
import Entity.Rosa;
import Entity.Formazione;
import Entity.Giocatore;

/**
 * Controlmain � una classe di tipo Control in cui � contenuto il main e richiama tutte le funzionalit� del sistema
 * Richiama la boundary men�_iniziale e gestisce la risposta dell'utente. In questa versione l'utente pu� solo scegliere il caso 
 * d'uso principale inserimento_formazione. 
 * Successivamente istanzia un oggetto della classe men�_iniziale per utilizzarne i metodi e carica la rosa del fantallenatore
 * 
 *  
 * 
 * @author gruppo 3
 * @version 2
 * @since 16/7/2020
 *
 */

public class Controlmain {

	public static void main(String[] args) throws IOException {
		
		
		Menu_iniziale scelta = new Menu_iniziale();
		Formazione_controller controller = new Formazione_controller ();
		
		if(scelta.formazione_inseribile()==1) {
		
			switch (scelta.scelta_utente()) {
			
			case 1: 
				controller.inserisci_formazione();
				//controller.stampavideoformazione();
				controller.salva_formazione("GarraCharrua_Titolari.txt");
				break;
			case 2:
				scelta.scelte_vietate();
				break;
			case 3:
				scelta.scelte_vietate();
				break;
			}
	}
		else scelta.tempo_scaduto();
	}
	
	
}
