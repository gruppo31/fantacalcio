PORTIERI
Gigio Donnarumma MILAN presenze parate
Antonio Donnarumma MILAN
Asmir Begovic MILAN

DIFENSORI
Martin Cacares FIORENTINA presenze passaggi
Cristian Zapata GENOA
Larangeira Danilo BOLOGNA 
Alex Sandro JUVENTUS
Mateo Musacchio MILAN
Diego Godin INTER
Henrique Dalbert FIORENTINA
Luiz Danilo JUVENTUS
Cristian Romero GENOA

CENTROCAMPISTI
Lucas Castro SPAL presenze passaggi goal
Matias Vecino INTER
Marques Allan NAPOLI
Nicolò Barella INTER
Rodrigo De Paul UDINESE
Douglas Costa JUVENTUS
Lucas Leiva LAZIO
Gaston Ramirez SAMPDORIA
Nathan Nandez CAGLIARI

ATTACCANTI
Luis Muriel ATALANTA presenze goal
Duvan Zapata ATALANTA
Joao Pedro CAGLIARI
Alexis Sanchez INTER
Lautaro Martinez INTER
Rafael Leao MILAN
Federico Santander BOLOGNA